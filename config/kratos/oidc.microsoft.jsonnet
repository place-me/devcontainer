local claims = {
  verified_primary_email: [],
} + std.extVar('claims');

{
  identity: {
    traits: {
      // Allowing unverified email addresses enables account
      // enumeration attacks, especially if the value is used for
      // e.g. verification or as a password login identifier.
      //
      // If connecting only to your organization (one tenant), claims.email is safe to use if you have not actively disabled e-mail verification during signup.
      //
      // The email might be empty if the account is not linked to an email address.
      // For a human readable identifier, consider using the "preferred_username" claim.
      
      [if "email" in claims then "email" else null]: claims.email,
      [if "family_name" in claims then "last_name" else null]: claims.family_name,
      [if "given_name" in claims then "first_name" else null]: claims.given_name,
      [if "preferred_username" in claims then "preferred_username" else null]: claims.preferred_username
    },
  },
}
