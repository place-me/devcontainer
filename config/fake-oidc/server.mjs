import { OAuth2Server } from 'oauth2-mock-server';

const server = new OAuth2Server();
await server.issuer.keys.generate('RS256');

const placemePayload = {
	tenant: process.env.TENANT ?? '39d6b2f2-8ad4-4e57-9b7c-1329340c3876',
	userprofile: process.env.USERPROFILE ?? 'daec4644-7341-4f74-a023-db2657362699',
	email: 'foo@bar.ch',
	firstname: 'Foo',
	lastname: 'Bar',
	permissions: ["userprofiles:view", "userprofiles:upsert", "assets:upsert", "userprofiles:delete", "assets:view", "assets:delete"],
};

server.service.on('beforeUserinfo', (userInfoResponse, req) => {
	userInfoResponse.body = {
		...(userInfoResponse.body ?? {}),
		...placemePayload
	};
});

server.service.on('beforeTokenSigning', (token, req) => {
	token.payload = {
		...(token.payload ?? {}),
		ext: { ...placemePayload },
	};

});

await server.start(8069, '0.0.0.0');
if (process.env.ISSUER) {
	server.issuer.url = process.env.ISSUER;
}
console.log('Local Issuer URL:', server.issuer.url);
console.log('ISSUER:', process.env.ISSUER ?? '39d6b2f2-8ad4-4e57-9b7c-1329340c3876')
console.log('USERPROFILE:', process.env.USERPROFILE ?? 'daec4644-7341-4f74-a023-db2657362699')
console.log('TENANT:', process.env.TENANT)