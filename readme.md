# devcontainer

This repo is intended to be used as a submodule in microservices within place.me.

To install, just add it as follows: `git submodule add git@gitlab.com:place-me/devcontainer.git .devcontainer/base`.

## place.me Services

| μ-service     | dapr-id          | dapr metrics port | Public Port | Private Port |
| :------------ | :--------------- | :---------------: | :---------: | :----------: |
| apigateway    | apigateway       |       9090        |    6868     |              |
| appcontextsvc | appcontextsvc    |       9090        |    6767     |              |
| bookingsvc    | bookingsvc       |       9090        |    8686     |              |
| hydra         |                  |                   |    4444     |     4445     |
| keto          | keto-admin-api   |       9091        |    4466     |     4467     |
| kratos        | kratos-admin-api |       9096        |    4433     |     4434     |
| login-client  | login-client     |       9090        |    6969     |              |
| mediasvc      | mediasvc         |       9090        | 6262 (http) | 6263 (gRPC)  |
| redirectsvc   | redirectsvc      |       9099        |    6161     |              |
| tagsvc        | tagsvc           |       9090        |    8585     |              |
| tokensvc      |                  |       9090        |    8484     |              |
| fake-oidc     |                  |                   |    8069     |              |

## Supporting Services

| μ-Service  | Public Port | Private Port |
| :--------- | :---------: | :----------: |
| placement  |             |    50000     |
| mailingsvc |    5555     |      25      |
| postgresd  |             |     5432     |
| redis      |             |     6379     |

## workspaces

Workspaces are predefined docker-compose services which can be used as service within a `devcontainer.json`. The docker-compose service name **must** be **workspace**. This is due to the way `network_mode` is working to link existing services.

- docker-compose-workspace-go.yml
- docker-compose-workspace-netcore.yml
