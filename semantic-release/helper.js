const mainBranchName = 'main';
const betaBranchName = 'develop';

const createBaseConfig = () => {
	const config = {
		branches: [
			mainBranchName,
			{
				name: betaBranchName,
				channel: 'beta',
				prerelease: 'beta'
			}
		],
		plugins: [
			['@semantic-release/commit-analyzer', {
				preset: "conventionalcommits",
			}],
			['@semantic-release/release-notes-generator', {
				preset: "conventionalcommits",
				presetConfig: {
					types: [
						{ type: "feat", section: "Features" },
						{ type: "fix", section: "Bug Fixes" },
						{ type: "chore", section: "Cleanup", hidden: false },
						{ type: "refactor", section: "Refactoring", hidden: false },
						{ type: "perf", section: "Performance", hidden: false }
					]
				}
			}],
			'@semantic-release/git',
			'@semantic-release/gitlab',
			'@semantic-release/exec'
		],
		ci: false,
		verifyConditions: [
			'@semantic-release/git',
			'@semantic-release/gitlab'
		],
		prepare: [
			{
				path: '@semantic-release/git',
				message: 'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}'
			}
		],
		publish: [
			'@semantic-release/gitlab'
		]
	};

	const currentBranch = process.env.CI_COMMIT_BRANCH;
	if (config.branches.some(it => it === currentBranch || (it.name === currentBranch && !it.prerelease))) {
		config.plugins.splice(2, 0, '@semantic-release/changelog');
		config.verifyConditions.splice(1, 0, '@semantic-release/changelog');
		config.prepare.splice(0, 0, '@semantic-release/changelog');
	}
	return config;
};

const addDockerSupport = (config) => {
	const registryPath = '${process.env.CI_REGISTRY}/${process.env.CI_PROJECT_PATH}/${process.env.CI_PROJECT_NAME}';
	config.verifyConditions.push({
		path: '@semantic-release/exec',
		verifyConditionsCmd: 'echo ${process.env.CI_REGISTRY_PASSWORD} | docker login -u ${process.env.CI_REGISTRY_USER} ${process.env.CI_REGISTRY} --password-stdin'
	});
	config.publish.push({
		path: '@semantic-release/exec',
		publishCmd: `docker push --all-tags ${registryPath}`
	});

	switch (process.env.CI_COMMIT_BRANCH) {
		case mainBranchName:
			config.prepare.push({
				path: '@semantic-release/exec',
				prepareCmd: `docker build -t ${registryPath}:latest -t ${registryPath}:` + '${nextRelease.version} .'
			});
		default:
			config.prepare.push({
				path: '@semantic-release/exec',
				prepareCmd: `docker build -t ${registryPath}:beta -t ${registryPath}:` + '${nextRelease.version} .'
			});
	}
	return config;
}

const addNugetSupport = (config) => {
	const ciJobToken = process.env.CI_JOB_TOKEN;
	const nugetRegistryPath = '${process.env.CI_SERVER_URL}/api/v4/projects/${process.env.CI_PROJECT_ID}/packages/nuget/index.json';

	config.prepare.push({
		path: '@semantic-release/exec',
		prepareCmd: `dotnet nuget add source ${nugetRegistryPath} \
                --name gitlab \
                --username gitlab-ci-token \
                --password ${ciJobToken} \
                --store-password-in-clear-text`
	});
	config.publish.push({
		path: '@semantic-release/exec',
		cmd: `dotnet nuget push dist/*.nupkg --source gitlab`,
	});
	config.publish[0] = {
		path: '@semantic-release/gitlab',
		assets: 'dist/*.nupkg',
	};
	return config;
}

const publishSubmodule = (config, submodulePath, pushUrl) => {
	config.prepare[0].assets = [`${submodulePath}`, 'CHANGELOG.md'];

	let prepare = `cd ${submodulePath} && git checkout main && `;
	prepare += 'if [ "$(git status --porcelain)" ] ; then git commit -am "chore(release): ${nextRelease.version} [skip ci]" ';
	prepare += '&& git tag v${ nextRelease.version }; ';
	prepare += 'fi';

	config.prepare.push({
		path: '@semantic-release/exec',
		prepareCmd: prepare
	});

	let publish = `cd ${submodulePath} && git checkout main && `;
	publish += `git push ${pushUrl} HEAD:main && `;
	publish += `git push --tags ${pushUrl}`;
	config.publish.push({
		path: '@semantic-release/exec',
		publishCmd: publish
	});
	return config;
}

/*
HACK: We should really be importing the semantic-release error package as below:
const SemanticReleaseError = require('@semantic-release/error');
and using that, however the import was failing when running semantic-release using npx
even when adding the @semantic-release/error package to npx.
Resorting to copying the source into this file instead
*/
class SemanticReleaseError extends Error {
	constructor(message, code) {
		super(message)
		Error.captureStackTrace(this, this.constructor)
		this.name = 'SemanticReleaseError'
		this.code = code
		this.semanticRelease = true
	}
}

module.exports = {
	createBaseConfig: createBaseConfig,
	addDockerSupport: addDockerSupport,
	addNugetSupport: addNugetSupport,
	publishSubmodule: publishSubmodule,
	mainBranchName: mainBranchName,
	betaBranchName: betaBranchName
}